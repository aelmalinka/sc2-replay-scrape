#!/bin/bash

die() {
	echo "$@" 1>&2
}

edo() {
	echo "$@"
	"$@" || die "$1 failed"
}

epip() {
	edo pip $@
}

epip install sc2reader
epip install numpy
epip install pandas
epip install pandas-datareader	# 2020-02-26 AMR NOTE: pandas (recommended by pacman)
epip install numexpr			# 2020-02-26 AMR NOTE: faster pandas (recommended by pacman)
epip install matplotlib			# 2020-02-26 AMR NOTE: graphing
