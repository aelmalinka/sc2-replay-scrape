#!/bin/bash

PACKDIR="$(pwd)/packs/"

zipfiles=(
	HomeStory_Cup_19_Replay_Pack-20200220T200609Z-001.zip
	HSC_XIII_Replay_Pack.zip
	IEM-Katowice-2019-Replaypack.zip
	IEM_Katowice_Replays.zip
	WESG2018_Grand_Finals_Replaypack.zip
	XG44SSN4GZKW1572888219002.zip
)
rarfiles=(
	HomeStory_Cup_XVII_Replay_Pack.rar
	IEM_XI_-_World_Championship_-_StarCraft_II_Replays.rar
)
zipfilesfix=(
	5DR7RFI2XJH91510960969376.zip
	BDSWARJ0UNJ61541302045188.zip
)
rarfilesfi=(
	HomeStory_Cup_XVI_Replay_Pack.rar
	HSCXV_Replay_Pack_\(RO16\).rar
)

die() {
	echo "$@" 1>&2
}

edo() {
	echo "$@"
	"$@" || die "$1 failed"
}

ezip() {
	edo unzip "${PACKDIR}/${1}"
}

ezipfix() {
	edo unzip "${PACKDIR}/${1}" -d "${1//.zip}"
}

erar() {
	edo unrar x "${PACKDIR}/$1"
}

erarfix() {
	mkdir "${1//.rar}"
	pushd "${1//.rar}"
	erar "${1}"
	popd
}

mkdir -p replays
pushd replays
for f in ${zipfiles[@]} ; do
	ezip "${f}"
done

for f in ${rarfiles[@]} ; do
	erar "${f}"
done

for f in ${zipfilesfix[@]} ; do
	ezipfix "${f}"
done

for f in ${rarfilesfix[@]} ; do
	erarfix "${f}"
done
popd

# 2020-02-27 AMR NOTE: added text file to HSC XVII
rm "replays/HomeStory Cup XVII Replay Pack/Day 1/Group B/ShaDoWn vs. Optimus - open for a surprise/Read me.txt"
# 2020-02-27 AMR NOTE: BW replays
rm -r "replays/BDSWARJ0UNJ61541302045188/Replays/BroodWar"
