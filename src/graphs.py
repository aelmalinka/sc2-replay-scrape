#!/usr/bin/env python
from argparse import ArgumentParser
from util import out_file, data_file
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
	parser = ArgumentParser()
	parser.add_argument(
		'-f,--file',
		dest='file',
		type=str,
		help='file to parse for graphs',
		default=data_file('SC2_Replays.csv')
	)

	args = parser.parse_args()

	df = pd.read_csv(args.file)

	df['matchup1'] = df['p1 race'] + 'v' + df['p2 race']
	df['matchup2'] = df['p2 race'] + 'v' + df['p1 race']

	df.loc[df['winner'] == df['p1'], 'matchup'] = df['matchup1']
	df.loc[df['winner'] == df['p2'], 'matchup'] = df['matchup2']

	df.loc[df['winner'] == df['p1'], 'winner apm'] = df['p1 apm']
	df.loc[df['winner'] == df['p2'], 'winner apm'] = df['p2 apm']

	df.loc[df['winner'] == df['p1'], 'loser apm'] = df['p2 apm']
	df.loc[df['winner'] == df['p2'], 'loser apm'] = df['p1 apm']

	wins = df['winner'].value_counts().to_frame(name='wins')

	apmdf = pd.DataFrame(columns = ['race', 'apm'])
	apmdf['race'] = df['p1 race'].append(df['p2 race'])
	apmdf['apm'] = df['p1 apm'].append(df['p2 apm'])

	apmdf = apmdf.reset_index()

	# 2020-02-29 AMR TODO: sort?
	_, ax = plt.subplots()
	df['matchup'].hist(ax=ax, bins=len(df['matchup'].value_counts()))
	ax.grid(False)
	plt.savefig(out_file('matchups.png'))

	_, ax = plt.subplots()
	df[['winner apm', 'loser apm']].plot(ax=ax, alpha=0.75)
	ax.grid(True)
	ax.set(
		xlabel='Game #',
		ylabel='Average APM'
	)
	plt.savefig(out_file('apmversus.png'))

	_, ax = plt.subplots(figsize=(6.6, 4.8))
	wins[wins['wins'] > wins['wins'].mean() + wins['wins'].std()].plot.barh(ax=ax)
	plt.savefig(out_file('wins.png'))

	_, ax = plt.subplots()
	apmdf.plot.scatter(x='race', y='apm', ax=ax)
	plt.savefig(out_file('apmraces.png'))
