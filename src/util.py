from os import environ,path,mkdir

if not path.exists('.cache'):
	mkdir('.cache')

# 2020-02-20 AMR TODO: tune
environ['SC2READER_CACHE_DIR'] = '.cache'
# environ['SC2READER_CACHE_MAX_SIZE'] = '100'

# 2020-02-20 AMR NOTE: supply tracker needs updating
# 2020-02-26 AMR NOTE: skipping creeptracker for now
# 2020-02-27 AMR TODO: fix creeptracker and supply tracker
# 2020-02-27 AMR TODO: SQ tracker?
# 2020-02-27 AMR TODO: make inject/chrono/mule tracker (energy of the affected?)
# 2020-02-28 AMR TODO: parse control-groups?
# 2020-02-27 AMR TODO: decode barcodes? somehow?
# 2020-02-28 AMR TODO: does remocing selection tracker improve perf? (at least until used)
from sc2reader.engine.plugins import (
	APMTracker
)

from sc2reader.engine import register_plugin

#register_plugin(SelectionTracker())
register_plugin(APMTracker())
#register_plugin(SupplyTracker())
#register_plugin(CreepTracker())

from sc2reader import load_replay
from math import log
from numpy import nan

# 2020-02-26 AMR TODO: late-game?
def SQ(income, unspent):
    # 2020-02-26 AMR NOTE: not recomended if income is less than 600
    if i < 600:
        return nan
    else:
        return 35 * ( 0.00137 * i - log(u)) + 240

def data_file(fname, dpath='data'):
    if not path.exists(dpath):
        mkdir(dpath)

    return path.join(dpath, fname)

def out_file(fname, opath='out'):
    if not path.exists(opath):
        mkdir(opath)

    return path.join(opath, fname)
