#!/usr/bin/env python
from argparse import ArgumentParser
from util import load_replay, data_file
from os import walk, path
import numpy as np
import pandas as pd

class Player:
    def __init__(self, replay, which):
        self.name = replay.teams[which - 1].players[0].name
        self.race = replay.teams[which - 1].lineup

        try:
            self.apm = replay.teams[which - 1].players[0].avg_apm
        except:
            print(f'apm failure for {self.name} in {replay.filename}')
            self.apm = np.nan

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('folder', type=str, help='replay file to load')

    args = parser.parse_args()

    # 2020-02-26 AMR TODO: barcodes
    # player, race, player, race, winner, map, length, p1 avg apm, p2 avg apm

    p1s = []
    p2s = []
    p1race = []
    p2race = []
    wins = []
    maps = []
    lens = []
    p1apm = []
    p2apm = []

    for root, subdirds, files in walk(args.folder):
        for file in files:
            fpath = path.join(root, file)
            print(f'loading {fpath}')

            try:
                replay = load_replay(fpath)

                if replay.type != '1v1':
                    print(f'invalid replay {fpath}')
                else:
                    player1 = Player(replay, 1)
                    player2 = Player(replay, 2)

                    if replay.winner is None:
                        winner = 'n/a'
                    else:
                        winner = replay.winner.players[0].name

                    p1s.append(player1.name)
                    p1race.append(player1.race)
                    p1apm.append(player1.apm)

                    p2s.append(player2.name)
                    p2race.append(player2.race)
                    p2apm.append(player2.apm)

                    wins.append(winner)
                    maps.append(replay.map_name)
                    lens.append(replay.game_length)
            except Exception as e:
                print(f'{fpath} failed to parse {e}')

    df = pd.DataFrame({
        'p1': p1s,
        'p1 race': p1race,
        'p2': p2s,
        'p2 race': p2race,
        'winner': wins,
        'map': maps,
        'length': lens,
        'p1 apm': p1apm,
        'p2 apm': p2apm
    })
    df.to_csv(data_file('SC2_Replays.csv'), index=False)
